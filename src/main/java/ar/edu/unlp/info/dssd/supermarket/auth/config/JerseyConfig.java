package ar.edu.unlp.info.dssd.supermarket.auth.config;

import org.glassfish.jersey.server.ResourceConfig;

public class JerseyConfig extends ResourceConfig {

	public JerseyConfig() {
		packages(new String[] { "ar.edu.unlp.info.dssd.supermarket.auth" });
	}
}
