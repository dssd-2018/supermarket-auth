package ar.edu.unlp.info.dssd.supermarket.auth;

import javax.ws.rs.core.Response;

import org.junit.Test;

import org.junit.*;

public class BonitaLoginTest {

	@Test
	public void testBonitaLogin() {
		JwtService jwtService = new JwtService();
		LoginInfo info = new LoginInfo();
		info.username = "admin";
		info.password = "1234";
		Response loginBonita = jwtService.loginBonita(info, AuthApi.BONITA_URL).build();
		
		Assert.assertTrue(loginBonita.getStatus() == 200);
		Assert.assertTrue(loginBonita.getHeaderString("X-Bonita-API-Token") != null);
	}

}
