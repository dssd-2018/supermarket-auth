FROM maven:3.3.9-jdk-8-alpine AS build-env

COPY pom.xml .

RUN ["mvn", "verify", "clean", "--fail-never"]

COPY . .

RUN ["mvn", "package", "-DskipTests"]

FROM gcr.io/distroless/java

COPY --from=build-env . .

ENTRYPOINT ["java", "-jar", "target/supermarket-auth.jar"]