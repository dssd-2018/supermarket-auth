# Supermarket Authentication

Authentication service for Supermarket-dssd
Based on a Java JWT service.

## Endpoints

- POST /auth/login
    - Description: Login, devuelve el token correspondiente.
    - Headers: Authorization Bearer
    - Body: { "username" : "", "password" : "" } 
    - Responses:
        - 200 OK 
        - 401 Usuario y/o contraseña incorrectos.

- POST /auth/authenticate
    - Description: Verifica el token ingresado, si es valido o no.
    - Headers: Authorization Bearer 
    - Responses: 
        - 200 OK
        - 401 Token inválido
        - 400 Token no ingresado

