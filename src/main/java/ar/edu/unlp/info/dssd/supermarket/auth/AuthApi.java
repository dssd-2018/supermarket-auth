package ar.edu.unlp.info.dssd.supermarket.auth;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import ar.edu.unlp.info.dssd.supermarket.auth.config.JwtConfig;

@Path("/")
public class AuthApi {

	final JwtService jwtService = new JwtService();

	public static final String BONITA_URL = "http://bpm.supermarket-dssd.ml";

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response info() {
		return Response.ok(new ApiInfo()).build();
	}

	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(LoginInfo info) {
		return Response.ok().header(JwtConfig.getHeader(), JwtConfig.getPrefix() + jwtService.getToken(info.username))
				.build();
	}

	@POST
	@Path("/login/bonita")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response loginBonita(LoginInfo info, @Context HttpHeaders headers) {
		if (headers.getHeaderString("X-Bonita-Location") != null)
			return jwtService.loginBonita(info, headers.getHeaderString("X-Bonita-Location")).build();

		return jwtService.loginBonita(info, BONITA_URL).build();
	}

	@POST
	@Path("/authenticate")
	@Produces(MediaType.APPLICATION_JSON)
	public Response authenticate(@Context HttpHeaders request) {
		if (jwtService.validate(request.getHeaderString(JwtConfig.getHeader())))
			return Response.ok().build();
		throw new NotAuthorizedException(Response.status(401).build());
	}

}
