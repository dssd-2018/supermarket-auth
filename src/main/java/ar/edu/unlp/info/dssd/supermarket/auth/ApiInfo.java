package ar.edu.unlp.info.dssd.supermarket.auth;


public class ApiInfo {
    public String deploy_timestamp = System.getenv("LAST_DEPLOY_TIMESTAMP");
    public String sha = System.getenv("COMMIT_SHA");
}