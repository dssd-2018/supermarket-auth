package ar.edu.unlp.info.dssd.supermarket.auth;

import java.util.Date;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import ar.edu.unlp.info.dssd.supermarket.auth.config.JwtConfig;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

public class JwtService {

	public final String getToken(String username) {
		Long now = System.currentTimeMillis();
		String token = Jwts.builder().setSubject(username)
				// Convert to list of strings.
				// This is important because it affects the way we get them back
				// in the Gateway.
				.setIssuedAt(new Date(now)).setExpiration(new Date(now + JwtConfig.getExpiration() * 1000)) // in
																											// //
																											// milliseconds
				.signWith(SignatureAlgorithm.HS512, JwtConfig.getSecret().getBytes()).compact();
		return token;
	}

	public final boolean validate(String token) {
		if (token == null || token.isEmpty())
			throw new BadRequestException();

		token = token.replace(JwtConfig.getPrefix(), "");

		try {
			Jwts.parser().setSigningKey(JwtConfig.getSecret().getBytes()).parse(token).getBody();
		} catch (ExpiredJwtException | SignatureException | UnsupportedJwtException | MalformedJwtException e) {
			System.out.println(token);
			System.out.println(e.getMessage());
			return false;
		}
		return true;
	}

	public final ResponseBuilder loginBonita(LoginInfo info, String location) {
		try {
			HttpResponse<JsonNode> loginResponse = Unirest.post(location + "/bonita/loginservice")
					.queryString("username", info.username).queryString("password", info.password)
					.queryString("redirect", false).header("Content-Type", "application/x-www-form-urlencoded")
					.asJson();
			if (loginResponse.getStatus() != 200) {
				return Response.status(loginResponse.getStatus());
			}
			if (loginResponse.getHeaders().containsKey("Set-Cookie"))
				for (String cookie : loginResponse.getHeaders().get("Set-Cookie")) {
					if (cookie.contains("X-Bonita-API-Token"))
						return Response.ok().header("X-Bonita-API-Token",
								cookie.split(":")[0].split("=")[1].split(";")[0]);
				}

		} catch (UnirestException e) {
			System.out.println("ERROR UNIREST: " + e.getMessage());
			return Response.serverError();
		}
		System.out.println("ERROR NO COOKIE");
		return Response.serverError();
	}

}
