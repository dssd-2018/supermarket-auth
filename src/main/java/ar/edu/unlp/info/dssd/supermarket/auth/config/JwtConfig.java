package ar.edu.unlp.info.dssd.supermarket.auth.config;

public class JwtConfig {

	public static int getExpiration() {
		return 24 * 60 * 60;
	}

	public static String getSecret() {
		// redis call for secret
		return System.getenv("JWT_SECRET") != null ? System.getenv("JWT_SECRET") : "c2VjcmV0";
	}

	public static String getHeader() {
		return "Authorization";
	}

	public static String getPrefix() {
		return "Bearer ";
	}

}
