package ar.edu.unlp.info.dssd.supermarket.auth;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;

public final class Application {

	private static final int SERVER_PORT = 8680;

	private Application() {
	}
	
	public static void main(String[] args) {

		Server server = new Server(SERVER_PORT);

		ServletContextHandler ctx = new ServletContextHandler(ServletContextHandler.NO_SESSIONS);

		ctx.setContextPath("/");
		server.setHandler(ctx);

		ServletHolder serHol = ctx.addServlet(ServletContainer.class, "/auth/*");
		serHol.setInitOrder(1);
		serHol.setInitParameter("jersey.config.server.provider.packages", "ar.edu.unlp.info.dssd.supermarket.auth");

		try {
			server.start();
			server.join();
		} catch (Exception ex) {
			Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			server.destroy();
		}
	}
}
